const globalres = require('../helper/Globalres');
const model = require('../models').user;
const upload = require('../helper/uploadfile')

class UserController {
    static async Register(req, res)  {
        try {
            req.body.photos = req.files.map(el => el.path)
            // insert files
            let insertdata = await model.create(req.body)
            return res.json({
                id_user : insertdata.id
            })

        } catch (error) {
            console.log(error);
            return res.status(500).json(globalres(500))
        }
    }

    static async search (req, res) {
        try {
            let {q, ob, sb, of, lt} = req.query
            
            let where = {}
            if (q !== undefined) {
                where = {
                    [q] : q 
                }
            }
            
            let data = await model.findAll({
                order: ob || 'name',
                offset: of || 0,
                limit: lt || 0
            })

            if (data.length < 1) {
                return res.status(404).json(globalres(404))
            }
            
            return res.json("ko")
        } catch (error) {
            console.log(error);
            return res.status(500).json(globalres(500))
        }
        
    }
}

module.exports = UserController