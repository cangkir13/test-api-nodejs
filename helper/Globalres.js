module.exports = (status, msg) => {
    let result = {}
    switch (status) {
        case 400:
                result = {
                    error : msg
                }
            break;
        case 401:
                result = {
                    error : "Invalid API key"
                }
            break;
        case 403:
            result = {
                error : "API key is missing"
            }
            break;
        case 404:
            result = {
                error : "User not found"
            }
            break;    
        case 500:
            result = {error:"Something went wrong. Please try again later."}
            break;        
        default:
            result = {
                error : "API key is missing"
            }
            break;
    }
    return result
}