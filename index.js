const express = require('express');
const app = express();
const bodyParser = require('body-parser');
const cors = require('cors')


const routes = require('./routers/routes')

/**
 * set .env as development || production
 */

const apikey = require('./middlewares/apikey');

// cors API
app.use(cors())

// for parsing application/xwww-
app.use(bodyParser.urlencoded({ extended: true }));
// parsing body json
app.use(bodyParser.json()); 

app.all('/user/*', apikey)


app.use(routes)

// not found endpoint
app.use(function (req, res, next) {
    return res.status(404).json({
        error:"No found endpoint"
    })
})

app.listen(3001, () =>{
    console.log(3001)
})
