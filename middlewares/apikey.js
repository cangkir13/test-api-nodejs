const globalres = require('../helper/Globalres');
module.exports = (req, res, next) => {
    let keys = "HiJhvL$T27@1u^%u86g";
    if (!req.header('key')) {
        return res.status(403).json(globalres(403))
    } else {
        let header = req.header('key')
        if (header !== keys) {
            return res.status(401).json(globalres(401))
        } else {
            return next()
        }
    }
}