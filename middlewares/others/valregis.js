const globalres = require('../../helper/Globalres');
const modeluser = require('../../models/').user
const modelType = require('../../models/').cctype
const moment = require('moment')
const {Op} = require('sequelize')

class validation {

    constructor() {}
    static async cekemail (email) {
        
        let data = await modeluser.findOne({
            where : { email }
        })

        return !data ? true : false
    }

    static async checkCCtype (str) {
        let data = await modelType.findOne({
            where : {
                name : str
            }
        })
        
        return !data ? false : true
    }

    static checkNumberCC(number) {
        if (number.length !== 16) {
            return false
        } else if(isNaN(number)) {
            return false
        }
        return true
    }

    static checkExpired(paaramsdate) {
        
        let dateformat = moment(paaramsdate, 'DD/MM/YYYY').format('DD/MM/YYYY') === paaramsdate
        
        if (!dateformat) {
            return {
                status : false, 
                message : 'creditcard_expired is must be DD/MM/YYYY'
            }
        } 
        let currentdate = moment()
        let dataRange = moment(paaramsdate,'DD/MM/YYYY', true).isAfter(currentdate)
        if (!dataRange) {
            return {
                status : false, 
                message : 'Your credit card is expired'
            }
        }

        return {
            status : true, 
            message : 'creditcard_expired is must be DD/MM/YYYY'
        } 
    }

    static async checkuserall (params) {
        let data = await modeluser.findOne({
            where : {
                [Op.or] : [
                    { creditcard_name : params.creditcard_name },
                    { creditcard_cvv : params.creditcard_cvv },
                    { creditcard_number : params.creditcard_number}
                ]
            }
        })
        
        return !data ? true : false
    }

    static async runval (req, res, next) {
        let body = req.body
        try {
            // validation email
        
            let cheemail = await validation.cekemail(body.email)
            if (!cheemail) {
                return res.status(400).json(globalres(400, "Please provide email feilds."))
            }

            // check type of credit card
            let CekCCty = await validation.checkCCtype(body.creditcard_type)
            if (!CekCCty) {
                return res.status(400).json(globalres(400, "Please check your type credit card."))
            }

            // check number credit card
            let cehcknumber = validation.checkNumberCC(body.creditcard_number)
            if (!cehcknumber) {
                return res.status(400).json(globalres(400, "Credit card number must be number type"))
            }

            // CHECK EXPIRED
            let chexpr = validation.checkExpired(body.creditcard_expired)
            if (!chexpr.status) {
                return res.status(400).json(globalres(400, chexpr.message))
            }

            // check account
            let account = await validation.checkuserall({
                creditcard_name : body.creditcard_name ,
                creditcard_cvv : body.creditcard_cvv ,
                creditcard_number : body.creditcard_number
            })
            if (!account) {
                return res.status(400).json(globalres(400, 'Credit card already exist'))
            }
            next()
        } catch (error) {
            console.log(error);
            return res.status(500).json(globalres(500))
        }
    }

}

module.exports = validation