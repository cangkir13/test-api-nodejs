/**
 * middleware joi verifiation data
 */
const globalresponse = require('../../helper/Globalres'); 
const validation = (schema, type) => {
    return (req, res, next) => {
        
        const { error } = schema.validate(req[type])
        const valid = error == null;
        
        if(valid) {
            return next();
        } else {
            const { details } = error;
            const message = details.map(e => e.message ).join(',');
            let response = globalresponse(400, `field ${message} `)
            return res.status(422).json(response)
        }
    }
}

module.exports = validation