const Joi = require('joi')

module.exports = {
    Register : Joi.object({
        name: Joi.string().required(),
        address: Joi.string().required(),
        email : Joi.string().email().required(),
        password : Joi.string().required(),
        photos : Joi.string().allow(""),
        creditcard_type : Joi.string().required(),
        creditcard_number : Joi.string().min(16).max(16).required(),
        creditcard_name : Joi.string().required(),
        creditcard_expired : Joi.string().required(),
        creditcard_cvv : Joi.string().required()
    }),
    search : Joi.object({
        q : Joi.string().allow(""),
        ob : Joi.string().valid('email', 'name'),
        sb : Joi.string().valid('asc', 'desc'),
        of : Joi.number().allow(""),
        lt : Joi.number().allow(""),
    })
}