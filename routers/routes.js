// const Bank = require('../controllers/controllerBank')
const validator = require('../middlewares/validator');
const validatorSchema = require('../middlewares/validator/schemas');
const router = require('express').Router()
const UserController = require('../controllers/UsersController')
const validationReq = require('../middlewares/others/valregis')

const upload = require('../helper/uploadfile');
const Globalres = require('../helper/Globalres');
const multer = require('multer');

const typeValidator = ['body', 'query', 'params']
// user route
router.post('/user/register', 
    upload.array('photos', 3),
    (error, req, res, nex) => {
        res.status(500).json(Globalres(500))
    },
    validator(validatorSchema.Register, typeValidator[0]), validationReq.runval, UserController.Register )

router.get('/user/list/', validator(validatorSchema.search, typeValidator[1]), UserController.search)

module.exports = router