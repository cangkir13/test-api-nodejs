'use strict';

const types = ['Gold', 'Platinum', 'Mastercard black', "Mastercard Platinum" ]
module.exports = {
  up: async (queryInterface, Sequelize) => {
    let datainsert = types.map(el => {
      return {
        name : el,
        createdAt: new Date(),
        updatedAt: new Date()
      }
    })
    return queryInterface.bulkInsert('cctypes', datainsert);
  },

  down: async (queryInterface, Sequelize) => {
    return queryInterface.bulkDelete('cctypes', null, {});
  }
};
